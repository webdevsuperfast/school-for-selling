(function($) {
	"use strict";

	$(document).ready(function(){
		// FitVids
		var container = $( '.video, .content' );
		container.fitVids();
		container.fitVids( {
			customSelector: "iframe[src*='//fast.wistia.net']"
		} );

		// Add empty class to empty p tags
		$('.content p').each(function() {
			var $this = $(this);
			if($this.html().replace(/\s| /g, '').length === 0){
				$this.addClass('empty');
			}
		});

		/* $('.testimonial').each(function(index, value){
			$('#testimonial-' + index + '').owlCarousel({
				singleItem:true,
				slideSpeed: 300,
				paginationSpeed: 400,
				navigation: false,
				pagination: true,
				navigationText: false,
				autoPlay: true,
				autoHeight: true
			});
		}); */

		$('#menu-primary-navigation').onePageNav({
			currentClass: 'active',
		    changeHash: false,
		    scrollSpeed: 750,
		    scrollThreshold: 0.5,
		    filter: '',
		    easing: 'swing',
		    begin: function() {
		        //I get fired when the animation is starting
		    },
		    end: function() {
		        //I get fired when the animation is ending
		    },
		    scrollChange: function($currentListItem) {
		        //I get fired when you enter a section and I pass the list item of the section
		    }
		});

		// Smooth Scrolling To Internal Links With jQuery
		// @link http://www.paulund.co.uk/smooth-scroll-to-internal-links-with-jquery
		$('a[href^="#"]').on('click',function (e) {
			e.preventDefault();

			var target = this.hash, $target = $(target);
			
			$('html, body').stop().animate({
				'scrollTop': $target.offset().top
			}, 900, 'swing');
		});
	});

	// Window load event with minimum delay
	// @https://css-tricks.com/snippets/jquery/window-load-event-with-minimum-delay/
	(function fn() {
		fn.now = +new Date;
		$(window).load(function() {
			if (+new Date - fn.now < 100) {
				setTimeout(fn, 100);
			}
		});
	})();
})(jQuery);
